//return something

function showMovie(age) {
    if (age < 18) {
        alert('Sorry, you can watch movie.');
        return age;    
    }
        
    alert('Showing you the movie');
    return age;
}

// let message = showMovie(10);
// console.log(message);

// let message = showMovie(20);
// console.log(message);

// showMovie(prompt("Your age"));

let yourAge = showMovie(prompt("Your age"));
console.log(yourAge);

// let yourAge = showMovie(parseInt(prompt("Your age")));
// console.log(yourAge);

// let yourAge = showMovie(20);
// console.log(yourAge);