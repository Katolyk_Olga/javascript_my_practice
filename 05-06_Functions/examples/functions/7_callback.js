//callback
function ask(question, yes, no) {

    let agree = confirm(question);

    if (agree === true) {
        yes()
    }
    else{
        no();
    } 
}

ask(
    'Do you agree?',
    function () { alert('You agreed.'); },
    function () { alert('You canceled the execution.'); }
);


//callback - компактний запис:
// function ask(question, yes, no) {
//     if (confirm(question) === true) yes()
//     else no() 
// }

// ask(
//     'Do you agree?',
//     function () { alert('You agreed.'); },
//     function () { alert('You canceled the execution.'); }
// );