function showMessage() {
  let message = 'Hello, I\'m JavaScript!';
  let arr = message.split('');
  //console.dir(console); // local variable 
  console.log(arr);
  alert(message);
}

showMessage(); // Hello, I'm JavaScript!
alert(message); // <-- Error! The variable is local to the function