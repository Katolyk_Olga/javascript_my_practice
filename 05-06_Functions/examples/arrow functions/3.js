let sum = (a, b) => { // the curly brace opens a multiline function 
  let result = a + b;
  return result; // if we use curly braces, use return to get results
};
alert(sum(1, 2)); // 