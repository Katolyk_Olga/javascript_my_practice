const createUser1 = (firstName, lastName) => {
  return {
    firstName,
    lastName,
  };
};

const createUser2 = (firstName, lastName) => ({
  firstName,
  lastName,
});

console.log(createUser1('Walter', 'White'));
console.log(createUser2('Walter', 'White'));
