let sum = (a, b) => a + b;
/* The arrow function is a shorter form of:
let sum = function(a, b) { 
    return a + b;
};
*/
alert(sum(1, 2)); // 


let double = n => n * 2;
// same as
// let double = function(n) { return n * 2 }
alert(double(3)); // 