const fruits = ['apple', 'orange', 'pineapple'];
const vegetables = ['cabbage', 'onion', 'tomato'];

/* spread operator in action */
const result = [...fruits, ...vegetables];
