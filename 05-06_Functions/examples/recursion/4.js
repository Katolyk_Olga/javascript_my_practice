function checkAge(age, granted, denied) {
	if (age < 18) denied();
	else granted();
}
let age = prompt('What is your age?', 18);
// function grantAccess() {
// 	alert('Access granted');
// }
// let grantAccess = () => alert('Access granted');
// function denyAccess() {
// 	alert('Access denied');
// }
// let denyAccess = () => alert('Access denied');
checkAge(
	age,
	() => alert('Access granted'),
	() => alert('Access denied') 
)

// function checkAge(...args) {
// function checkAge() { arguments
//}