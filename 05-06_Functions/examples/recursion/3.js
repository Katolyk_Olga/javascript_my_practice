const incomingNumber = +prompt("Please enter your number:")

function  isPrime(n) {
	for(let i = 2; i < n; i++) {
		if (n % i === 0) {
			return false;
		}	
	}
	return true;
}

function showPrimes(n) {
	if (n > 0) {
		console.log(1);
	}
	for (let i = 2; i <= n; i++) {
		if (isPrime(i)) {
			console.log(i);
		}
	}
}

showPrimes(incomingNumber)