// Напишіть функцію, яка приймає рядок і з малих літер робить великі,
// а великі - малими.
// example('hello world') => 'HELLO WORLD'
// example('HELLO WORLD') => 'hello world'
// example('hello WORLD') => 'HELLO world'
// example('HeLLo WoRLD') => 'hEllO wOrld'
// example('12345') => '12345'
// example('1a2b3c4d5e') => '1A2B3C4D5E'