// Ваша мета – створити функцію, яка видаляє перший та останній символи рядка.
// Вам дається один параметр – вихідний рядок.

// Приклад коду:
// removeChar('eloquent') => 'loquen'
// removeChar('country') => 'ountr'
// removeChar('person') =>'erso'
// removeChar('place') =>'lac'