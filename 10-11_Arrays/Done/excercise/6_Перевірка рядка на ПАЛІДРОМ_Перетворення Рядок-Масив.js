// Створити функцію, яка перевіряє, чи є переданий у неї рядок
// Паліндром (анна це паліндром).
// анна
// Алла
// дерево
// Курка — біб, а крук
// А муза рада музе без ума да разума
// "Пустите!" - Летит супу миска Максиму. - "Пустите, летит суп!"
// 12321
// 5446456588

// Підказка: потрібно перетворити рядок на масив

function isPalindrome (str) {
    // const newStr = str.toString().toLowerCase().replace(/\s|[,.?!/—"-]/g,"")
    const newStr = String(str).toLowerCase().replace(/\s|[,.?!/—"-]/g,"")
    return newStr === newStr.split("").reverse().join("")
}

console.log(isPalindrome('"Пустите!" - Летит супу миска Максиму. - "Пустите, летит суп!"'));
console.log(isPalindrome('This is not a palindrome'));
console.log(isPalindrome(20));