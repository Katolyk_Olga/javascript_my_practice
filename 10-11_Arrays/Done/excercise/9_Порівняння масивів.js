// Опис завдання: Напишіть функцію, яка порівнює два масиви і повертає true,
// якщо вони ідентичні.
// Очікуваний результат: ([1, 2, 3], [1, 2, 3]) => true

const similarArrs = (n, m) => {
    // if (n.length === m.length && n.every((el, index) => el == m[index])) {
    //     return true;
    // } else {
    //     return false;
    // }
    const solution = (n.length === m.length && n.every((el, index) => el == m[index]));
    return solution ? true : false;
}



console.log(similarArrs([1, 100, 'aef', false], [1, 100, 'aef', false]));
console.log(similarArrs([1, 100, 'aef', false], [1, 100, 1000, 10]));
console.log(similarArrs(['333', '23'], ['333', 23]));
