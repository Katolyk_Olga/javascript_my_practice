/**
	* Завдання 2.
	*
	* Написати мульти-мовну програму-органайзер.
	*
	* Програма запитує у користувача якою мовою він бажає побачити список днів тижня.
	* Після введення користувачем бажаної мови програма виводить у консоль дні тижня вказаною мовою.
	*
	* Доступні мови (локалі): ua, de, en.
	*
	* Якщо введена користувачем локаль не збігається з доступними - програма перепитує його доти,
	* Доки доступна локаль не буде введена.
	*
	* Умови:
	* - Рішення має бути коротким, лаконічним та універсальним;
	* - всіляко використовувати методи масиву;
	* - Використання методу Object.keys() є обов'язковим.
	*/

/* Дано */
const days = {
	ua: [
		'Понеділок',
		'Вівторок',
		'Середа',
		'Четвер',
		'П\'ятниця',
		'Субота',
		'Неділя',
	],
	de: [
		'Montag',
		'Dienstag',
		'Mittwoch',
		'Donnerstag',
		'Freitag',
		'Samstag',
		'Sonntag',
	],
	en: [
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday',
	],
	fr: [
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday',
	],
};

/* Рішення */
let avalLang = Object.keys(days);
let language = prompt(`Enter language: ${avalLang.join(", ")}.`).toLowerCase();
// language = language.toLowerCase();

while(!avalLang.includes(language)) {
	language = prompt(`Incorrect language, try again: ${avalLang.join(", ")}.`).toLowerCase();
   
} 

days[language].forEach(element => {
	console.log(element);
});

