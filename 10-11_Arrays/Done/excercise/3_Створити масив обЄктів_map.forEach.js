// Створити масив об'єктів students (у кількості 7 прим).
// У кожного студента має бути ім'я, прізвище та напрямок навчання - Full-stack
// чи Front-end. Кожен студент повинен мати метод, sayHi, який повертає рядок
// `Привіт, я ${ім'я}, студент Dan, напрям ${напрямок}`.
// Перебрати кожен об'єкт і викликати у кожного об'єкта метод sayHi;

const students = [{
    name: "Andrew",
    lastName: "Wyatt",
    direction: "Front-end",
},
{
    name: "Alex",
    lastName: "Jones",
    direction: "Front-end",
},
{
    name: "James",
    lastName: "Doe",
    direction: "Front-end",
},
{
    name: "Alex",
    lastName: "Green",
    direction: "Front-end",
},
{
    name: "Andrew",
    lastName: "Doe",
    direction: "Full-stack",
},
{
    name: "Alex",
    lastName: "Doe",
    direction: "Full-stack",
},
{
    name: "Andrew",
    lastName: "Smith",
    direction: "Full-stack",
}
]

students.map((student) => student.sayHi = function() {
    return `Привіт, я ${this.name} , ${this.lastName}, студент Dan, напрям ${this.direction}`
})

// students.map((student, index) => student.sayHi = () => {
//     return `Привіт, я ${students[index].name} , ${students[index].lastName}, студент Dan, напрям ${students[index].direction}`
// })

console.log(students);
students.forEach((student) => console.log(student.sayHi()));

user.name = 'John';

user.setName('John');