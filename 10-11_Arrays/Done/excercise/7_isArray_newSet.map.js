// Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам потрібно отримати новий масив, об'єктів типу
// {
// type: 'car'
// brand: ${елемент масиву}
// }

// Для всіх машин додати властивість isElectric, для Tesla воно буде true, 
// для решти false;
// Для того щоб перевіряти що електрична машина створити окрему функцію,
// що буде зберігати у собі масив брендів електричних машин 
// і перевірятиме вхідний елемент - якщо він у масиві.

// Створити новий масив, у якому зберігаються всі машини, що є електрокарами.
// Вивести його в модальне вікно користувачеві.

// let cars = ["bMw", "Audi", "teSLa", "toYOTA"];
let moreCars = ["bMw", "Audi", "teSLa", "toYOTA", "bMw", "Audi", "teSLa", "toYOTA", "bMw", "Audi", "teSLa", "toYOTA"]
let createObjList = (arr) => {
    if (Array.isArray(arr)){
        return Array.from(new Set(arr)).map((car,index) => (
            {
                id: index,
                type: 'car',
                brand:car,
                isElectric: electricCars(car)
            }
        ))
    }else {
        return `Error! ${arr} is not an array!`;
    }
}

const electricCars = (car) => {
    const eCars = ["tesla"];
    return eCars.includes(car.toLowerCase());
}

console.log(createObjList(moreCars));
