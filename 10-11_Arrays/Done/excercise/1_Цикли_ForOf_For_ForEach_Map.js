/**
  * Завдання 1.
  *
  * Даний масив з днями тижня.
  *
  * Вивести в консоль усі дні тижня за допомогою:
  * - Класичного циклу for;
  * - Цикла for...of;
  * - Спеціалізований метод для перебору елементів масиву forEach.
  * Вивести дні тижня починаючи з понеділка, зараз починаються з неділі
  */

/* Дано */
const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];

/* Рішення */
let sun = days.shift();
days.push(sun);
//days.push(days.shift());

// for (let i = 0; i < days.length; i++) {
//   const element = days[i];
//   console.log(element);
// }

// for (let day of days) {
//   console.log(day);
// }

const a = days.forEach((elem, i, arr) => elem);
const b = days.map((elem, i, arr) => elem);
console.log(a, b);