// Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам потрібно отримати новий масив, об'єктів типу
// {
// type: 'car'
// brand: ${елемент масиву}
// }

// Вивести масив у консоль

let cars = ["bMw", "Audi", "teSLa", "toYOTA"];

let createObjList = (arr) => {
    if (Array.isArray(arr)){
        return arr.map((car,index) => (
            {
                id: index,
                type: 'car',
                brand:car
            }
        ))
    }else {
        return `Error! ${arr} is not an array!`;
    }
}

console.log(createObjList(cars));
console.log(createObjList(10));






























const test = {
    _a:'532665326532',
    b: '54664',
    set a(value) {
        if(typeof value === 'string'){
            return this._a = value
        }
    },
    get a(){
        return this._a
    }

}

// console.log(test.a); // numbver
// test.a = 874358743464564
// console.log(test.a); // string
// console.log(test._a);
// test._a = 'dsfdsfdsfdsfdsffds'
// console.log(test._a);
// console.log(test.a);
//
