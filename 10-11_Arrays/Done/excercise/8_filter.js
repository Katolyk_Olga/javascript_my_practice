// Опис завдання: Напишіть функцію, яка очищає масив від небажаних значень,
// Таких як false, undefined, порожні рядки, 0, null.

const filterFn = (elem) => {
    if(Array.isArray(elem)) {
        return elem.filter((item) => {
            if(Boolean(item)) {
                return item;
            }
        })
    } else {
        return `${elem} ${typeof elem}`
    }

}
console.log(filterFn([0, 1, undefined, '', false, 2, undefined, null, 3, NaN, null]));
console.log(filterFn());
console.log(filterFn(243));