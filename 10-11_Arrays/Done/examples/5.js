//Перетворення масивоподібних обєктів в масив

// let mystr = '𝒳😍 ';
// splits mystr into array of characters, taking into account surrogate pairs
// let chars = Array.from(mystr);
// console.log(chars[0]); // 𝒳
// console.log(chars[1]); // 😍
// console.log(chars.length); // 

//Сет
//Об'єкт Set дозволяє зберігати унікальні 
//значення будь-якого типу, будь то примітиви або посилання на об'єкти.

// let set = new Set();
// let john = { name: "John" }; 
// let peter = { name: "Peter" }; 
// let mary = { name: "Mary" };
// // visits, some users come multiple times 
// set.add(john);
// set.add(peter); 
// set.add(mary); 
// set.add(john); 
// set.add(mary);

// console.log(set);
// // set keeps only unique values 
// console.log(set.size); // 3
// for (let user of set) {
// console.log(user.name); // John (then Peter and Mary)
// }
// console.log(set);


// //Мап
// //Об’єкт Map містить пари ключ-значення та 
// // запам’ятовує вихідний порядок вставки ключів. 
// // Будь-яке значення (як об’єкти, так і примітивні значення) 
// // можна використовувати як ключ або значення.

// let map = new Map(); 

// map.set('1', 'str1');
// map.set(1, 'num1');
// // a string key
// // a numeric key
// map.set(true, 'bool1'); // a boolean key
// // Map keeps the key type (unlike Object), so these two are different: 
// console.log(map.get(1)); // 'num1'
// console.log(map.get('1')); // 'str1'
// console.log(map); // 3
// // Using objects as keys
// let user = { name: "John" };
// // let's store the number of visits for every user, 
// let visitsCountMap = new Map();
// // john is the key for the map 
// visitsCountMap.set(user, 123);
// console.log(visitsCountMap.get(user)); // 12

// console.log(map);