//Spread Operator

// let arr = [3, 5, 1];
// console.log(arr);
// console.log(...arr);

// console.log(Math.max(...arr));//3, 5, 1


let arr1 = [1, -2, 3, 4];
// let arr2 = [8, 3, -8, 1];
// console.log(Math.max(1, ...arr1, 2, ...arr2));


// let merged = [0, ...arr1, 2, ...arr2];
// let arr1Copy = [...arr1];
// arr1.push(10);
// console.log(arr1, arr1Copy);

// let str = "Hello";
// console.log([...str]); // H,e,l,l,o

// //Видалення елементів масиву
// arr.splice(index[, deleteCount, elem1, ..., elemN])

// let arr = ["I", "study", "JavaScript", "basic"];
// let diff = arr.splice(1, 2, ...arr1);
// console.log(diff)// from index 1 remove 1 element
// console.log(arr); // ["I", "JavaScript"]

// //arr.slice(start, end)
let arr = ["This", "is", "a", "test", "I", "study", "JavaScript", "basic"]; 
// console.log(arr.slice(0), arr);
console.log(arr.slice(1, -2), arr); // is,a 
// console.log(arr.slice(-2), arr); // a,test

