//Методи масивів


//Сортування
// let arr = [1, 2, 15, 3, -2, 12];
// user = {
//     name:'test'
// }
// console.log(arr);
// arr.sort();
// console.log(arr);// 1, 15, 2

// function compare(a, b) {
//     console.log("a = " + a,"b = " + b);

//     if (a < b) {
//         return -1
//     };

//     if (a == b) return 0;

//     if (a > b) return 10;
// }

// arr.sort(compare);
// console.log(arr);


// // arr.sort(function (a, b) { return a - b; });
// // console.log(arr);// 1, 2, 15

// arr.sort((a, b) => {
//     console.log('a='+a,'b='+b);
//     return a - b
// });

//console.log(arr);// 1, 2, 15

// //Пошук в масиві

// //індекс необхідного елемента
// let arr = [1, 0, false, 0];
// console.log(arr.indexOf(0)); // 1
// console.log(arr.indexOf(false)); // 2
// console.log(arr.indexOf(null)); // -1
// console.log(arr.includes(1)); // true

// //Метод arr.find()
// let users = [
//     { id: 1, name: "John" },
//     "test",
//     { id: 2, name: "Pete" },
//     { id: 3, name: "Mary" }
// ];

// let user = users.find((item, index, arr) => {
//     console.log(item, index, arr);
//     return item.id == 3
// });
// console.log(user); // John

// //Метод Фільтр
// let users = [
//     { id: 1, name: "John" },
//     { id: 2, name: "Pete" },
//     { id: 3, name: "May" }
// ];
// // // returns array of the first two users
// let someUsers = users.filter(item => item.name.length > 3);
// console.log(someUsers.length);
// console.log(someUsers); // 
// let arrRand = ["Bilbo", "Gandalf", "Nazgul"];
// //Перетворення масиву
// let lengths = arrRand.map(item => {
//     if(item.length > 5) {
//         return item.length;
//     }
//     return null;
// });
// console.log(lengths, arrRand);

//Розбивка і склеювання в масив
// let names = 'Bilbo, Gandalf, Nazgul'; // 20/10/2023

// let arr = names.split(', ');

// console.log(arr);

// for (let name of arr) {
//     console.log(`A message to ${name}.`); // A message to Bilbo (and other names)
// }

let arr = ['Bilbo', 'Gandalf', 'Nazgul'];
let str = arr.join(': ');
console.log(str); // Bilbo;Gandalf;Nazgul
