//Перебір масиву
// let fruits = ['Apple', 'Orange', 'Melon'];

// for (let i = 0; i < fruits.length; i++) {

//     console.log(fruits[i]); // Apple, Orange, Melon

// }

// // //або

// for (let fruit of fruits) {

//     console.log(fruit); // Apple, Orange, Melon

// }


//Додавання/видалення елементів масиву
// let fruits = ['Apple', 'Orange'];
// fruits.push('Melon');
// console.log(fruits); // Apple, Orange, Melon
//delete fruits[0];
//console.log(fruits);
// console.log(fruits.shift()); // remove Apple and console.log it
// console.log(fruits); // Orange, Melon


// let fruits = ['Apple', 'Orange'];
// fruits.push('Pear');
// console.log(fruits); // Apple, Orange, Pear

// console.log(fruits.pop()); // remove "Pear" and console.log it 
// console.log(fruits); // Apple, Orange

let matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];
console.log(matrix[1][1]);