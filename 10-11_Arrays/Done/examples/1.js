//Створення масиву

let arr = new Array(4);
let arr2 = []; // more common

//console.dir(Array);
// console.log(arr, arr2);
//Масив рядків
let fruits = ['Apple', 'Orange', 'Melon'];

// //Масив різних типів даних
let arr3 = ['Apple', { name: 'John',age : 32 }, true, function () { console.log('hello'); }];

//Зчитування елементів масиву
// console.log(fruits[0]); // Apple 
// console.log(fruits[1]);  
// console.log(arr3[1]()); 

// //Присвоєння нового значення конкретному елементу
// fruits[2] = 'Pear'; // now ['Apple', 'Orange', 'Pear'] 
// console.log(fruits);
// {
//     0: 'Apple',
//     1: 'Orange',
//     2: 'Pear'
// }
// //Додавання елементу якщо такого ще не існувало
// fruits[6] = 'Lemon'; // now ['Apple', 'Orange', 'Pear', 'Lemon']
// console.log(fruits);
// // //Кількість елементів масиву можна отримати з вже відомої нам властивості
// console.log(fruits.length, fruits);

// //Довжина це індекс останнього елемента масива + 1
// let fruits2 = []; 
// fruits2[123] = 'Apple';
// console.log(fruits2.length); // 124


// //Довжину масива можна переписати таким чином обрізавши його чи додавши порожні елементи
let arr4 = [1, 2, 3, 4, 5];
arr4.length = 2; // truncate to 2 elements 
console.log(arr4); // [1, 2]


