// У нас є об'єкт зберігання зарплат нашої команди:
// → Підсумуйте всі зарплати!
// → Має бути 390
// → Якщо зарплати порожні, то результат має бути 0

let salaries = {
    John: 100,
    Ann: 160,
    Peter: 130
}

