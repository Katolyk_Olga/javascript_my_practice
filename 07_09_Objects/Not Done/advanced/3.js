/**
  * Завдання 3.
  *
  * Написати імплементацію методу Object.freeze().
  *
  * Метод повинен:
  * - запобігати можливості додавання нових властивостей до об'єкта;
  * - запобігати можливості видалення існуючих властивостей об'єкта;
  * - запобігати можливості зміни дескрипторів властивостей об'єкта;
  * - Запобігати можливості зміни значення властивостей об'єкта.
  *
  * Умови:
  * - Вбудованим методом Object.freeze() користуватися заборонено;
  * - Вбудованим методом Object.preventExtensions() можна користуватися.
  */