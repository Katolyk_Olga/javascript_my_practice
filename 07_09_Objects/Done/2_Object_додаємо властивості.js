/**
  * Завдання 3.
  *
  * Розширити функціонал об'єкта з попереднього завдання:
  * - Додати метод, який додає об'єкту нову властивість із зазначеним значенням.
  *
  * Просунута складність:
  * Метод має бути «розумним» - він генерує помилку при створенні нової властивості
  * Властивість з таким ім'ям вже існує.
  */

const user = {
  firstName : 'John',
  secondName : 'Smit',
  job : 'driver',
  sayHi() {
    alert(`Hello. My name is ${this.firstName} ${this.secondName}.`);
  },

  updateProp(propName, propValue) {
    if (propName in this) { // this[propName] 
      this[propName] = propValue;//this["firstName"] = "Andrii"
    }
    else {
      throw new Error(`${propName} не існує`)
    }
  },

  addProp (propName, propValue) {
    if (propName in this) {
      throw new Error(`Error! ${propName} already exists!`)
    } else {
      this[propName] = propValue;
    }
  }
}
// user.updateProp('hobbies', "Football");

//let x = prompt() якщо хочемо отримати іформацію від юзера

user.addProp("firstName", "Soccer");
// user.updateProp('hobbies', "Football");
console.log(user);