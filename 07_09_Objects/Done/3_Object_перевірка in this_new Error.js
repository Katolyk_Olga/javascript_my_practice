/**
  * Завдання 2.
  *
  * Розширити функціонал об'єкта з попереднього завдання:
  * - Метод sayHi повинен вивести повідомлення: «Привіт. Мене звуть Ім'я Прізвище.»;
  * - Додати метод, який змінює значення зазначеної властивості об'єкта.
  *
  * Просунута складність:
  * Метод має бути «розумним» — він генерує помилку під час спроби
  * Зміни значення неіснуючої в об'єкті властивості.
  */

const user = {
  firstName : 'John',
  secondName : 'Smit',
  job : 'driver',
  sayHi() {
    alert(`Hello. My name is ${this.firstName} ${this.secondName}.`);
  }
}

user.updateProp = function (propName, propValue) {
  if (propName in this) { // this[propName] 
    this[propName] = propValue;//this["firstName"] = "Andrii"
  }
  else {
    throw new Error(`${propName} не існує`)
  }
  
  
}

user.sayHi();

user.updateProp("firstName", "Andrii");
user.updateProp("secondName", "Ku");
user.updateProp("job", "IT");
user.updateProp("hobbies", "Makrame");

console.log(user);