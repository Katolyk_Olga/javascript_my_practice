'use strict';
//Тип Symbol()

// let id1 = Symbol("id");
// let id2 = Symbol("id");
// alert(id1 == id2); // false


// let user = {
//     name: "John"
// };

// let id = Symbol("id");

// let id2 = Symbol("id");

// user[id] = "ID Value";

// user[id2] = "unique Value";

// console.log(user); // we can access the data using the symbol as the key


let id = Symbol("identifier");

let user = {
    name: "John",
    age: 30,
    [id]: 123 // not just "id: 123"
};

console.log(user)

for (let key in user) {
    console.log(key); // This won't output the symbol key
}

const obj2 = Object.assign({}, user);

console.log(obj2);
// const uniqueKey = Symbol('uniqueKey');

// const obj = {
//   [uniqueKey]: 'This property is identified by a symbol'
// };

// for (let key in obj) {
//   console.log(key); // This won't output the symbol key
// }

// const obj2 = Object.assign({}, obj);
// console.log(obj2);
// console.log(Object.keys(obj)); // This won't include the symbol key