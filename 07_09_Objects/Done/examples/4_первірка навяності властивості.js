//Перевірка властивостей

//let user = {};

//user.noSuchProperty = null;

//alert(user.noSuchProperty === undefined); // true means "no such property"

let user = { name: "John", age: 30 };

alert('age' in user); // true, user.age exists

alert("blabla" in user); // false, user.blabla doesn't exist
