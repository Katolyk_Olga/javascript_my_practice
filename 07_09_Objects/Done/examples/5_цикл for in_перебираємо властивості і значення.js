//цикл for in 

let user = {
    name: "John",
    age: 35,
    isAdmin: true
};

let q = 0;

for (let item in user) {
    // keys
    console.log(item); // name, age, isAdmin
    // values for the keys
    console.log(user[item]); // John, 30, true

    q++;  
}

console.log(q);