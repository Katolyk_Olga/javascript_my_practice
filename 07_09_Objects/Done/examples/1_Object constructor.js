//let user = new Object(); // "object constructor" syntax 
//let user = {}; // "object literal" syntax

let user = { // an object
    name: 'John', // by key "name" store value 'John' 
    age: 30// by key "age" store value 30
};
console.log(user);