//Копіювання об'єктів по посиланню

// let user = { name: "John" };
// let admin = user; // copy the reference
// admin.name = "David"; // changed by the "admin" reference
// user.age = 30;
// admin.age = 35;
// console.log(user, admin); // "David", changes are seen from the "user" reference


// let a = {};
// let b = a; 
// alert(a == b); 
// alert(a === b); 


let a = {};
let b = {}; 
alert(a == b); 