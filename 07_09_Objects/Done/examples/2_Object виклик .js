//Виклик через квадратні дужки
let user = {
    name: "John",
    age: 30,
    "something new": 'test'
};
let key = prompt("What do you want to know about the user?", "name");
// access by variable
console.log(user.name, user['age'])
// alert(user[key]);// John (if enter "name") user["name"]
// alert(user.key); // user."name" != user.name
alert(user["something new"]);