//Клонування об'єктів

let user = {
    name: "John",
    age: 30,
    hobbies: {
        sports: 'football',
        tableGames: 'monopoly',
    }
};

let job = {
    age: 35,
    role: 'developer',
    grade: 'senior',
};

// let clone = Object.assign({age:32, hobby: 'sports'}, user, job);

//let clone = Object.assign({age:32, hobby: 'sports'}, job, user);

let clone = Object.assign({}, user);
// clone.name = "David"; 
// console.log(user); 

clone.name = "Joe";
clone.hobbies.sports = 'tennis';

console.log('clone', clone); 

console.log('user', user);

console.dir(Object);