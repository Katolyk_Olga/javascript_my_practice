//Короткий запис
const name = "John";
const age = 30;

// let user = {
//     name: name,
//     age: age
// };
// alert(user.name); // John

// Скорочуємо запис якщо назва властивості і значення співпадають
// let user = {
//     name, // same as name: name 
//     age // same as age: age
// };
// alert(user.name); // John

const user = {
    age: 35,
    name, // same as name: name
    
};
console.log(user)
///
user.name = "Joe";

console.log(user)