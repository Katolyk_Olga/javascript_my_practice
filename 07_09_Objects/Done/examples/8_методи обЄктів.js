'use strict';
//Методи об'єктів

// let user = {
//     name: "John",
//     age: 30
// };

// user.sayHi = function () {
//     alert(`Hello! ${this.name}`);
// };

// user.sayHi(); // Hello



// sayHi()
//Короткий запис

// let user = {
//     name: "John",
//     age: 30,
//     sayHi: function (param) {
//         alert("Hello");
//     }
// };

// function sayHello() {
//     console.log(this);
// };
// sayHello()
// let user2 ={}
let user = {
    name: "John",
    age: 30,
    sayHi() { // same as "sayHi: function()"
        console.log(this.name);
    }
};
 user.sayHi()