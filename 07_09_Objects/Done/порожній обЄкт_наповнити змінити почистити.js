// Створіть порожній об’єкт product
// Додайте властивість name зі значенням «Laptop».
// Додайте властивість price зі значенням c
// Змініть значення price на 1000
// Показати назву продукту та ціну на екрані
// Видалити name властивість з об’єкта

let price = 600;

const product = {};
product.name = "Laptop";
product["price"] = 1200;
product.price = 1000;
alert(product.name + " " + product["price"]);
//alert(`${product.name} ${product["price"]}`);
delete product.name;
console.log(product);