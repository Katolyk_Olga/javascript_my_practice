/* Завдання 4.
 *
 * Напишіть об'єкт calendar з такими методами
 * 
 * curday щоб отримати поточну дату.
 * 
 *
 * приклад :
 * console.log(calendar.curday('/'));
 * "11/13/2014"
 * console.log(calendar.curday('-'));
 * "11-13-2014"
 * 
 * numberOfDays повертає кількість днів у місяці.
 * Два параметри:
 * - місяць
 * - рік
 * 
 * 
 * повертає назву дня тижня (словом),
  * яка була вказана кількість днів тому.
  *
  * Функція володіє одним параметром:
  * - Число, яке описує, скільки днів тому ми хочемо повернутися, щоб дізнатися 
    * бажаний день.
  *
  * Умови:
  * - використання об'єкта дата обов'язково.
  *
  * Замітка:
  * - Можна використовувати готовий об'єкт з днями тижня.
  * 
  * 
 */
const days = {
  0: 'Неділя',
  1: 'Понеділок',
  2: 'Вівторок',
  3: 'Середа',
  4: 'Четвер',
  5: 'П\'ятниця',
  6: 'Субота',
};



const calendar = {
  curday(view) {
    let today = new Date();// now moment
    //console.log(today);
    let day = today.getDate();

    if (day < 10){
      day = '0' + day;
    }

    let month = today.getMonth() + 1;

    if (month < 10){
      month = '0' + month;
    }

    let year = today.getFullYear();

    let date = day + view + month + view + year;

    return date;

  },
  numberOfDays(month, year) {

    let today = new Date(year, month, 0);

    return today.getDate();

  },
  getDayAgo (daysAgo = 0) {
    let date = new Date ();
    date.setDate(date.getDate() - daysAgo);

    return days[date.getDay()];
  }
}
console.log(calendar.getDayAgo(4));

console.log(calendar.numberOfDays(2, 2005));

console.log(calendar.curday('-'));
console.log(calendar.curday(':'));
console.log(calendar.curday('/'));

// let randomDate = '2005-11-02';
// let randomYear = randomDate.slice(0, 4);
// let randomMonth = randomDate.slice(5,7);
// console.log(`year ${randomYear}, month ${randomMonth}`);
//let randomDate = '   2005-11-02    '.trim();