/**
  * Завдання 4.
  *
  * За допомогою циклу for...in вивести в консоль усі властивості
  * першого рівня об'єкта у форматі «ключ-значення».
  *
  * Просунута складність:
  * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
  */

/* Дано */
const user = {
	firstName: "John",
	surname: "Black",
	job: "developer",
	key: true,
	hobbies: {
		sport: "footbal",
		collect: "coins",
		tableGame: "chess"
	},
	salary: null,
	sayHi() {
		console.log(`Привет. Меня зовут ${this.firstName} ${this.surname}.`);
	},
	updateProperty(key, value) {
		if (key in this) {
			this[key] = value;
		}
		else {
			throw new Error(`Error! ${key} not found`)
		}
	}

}



