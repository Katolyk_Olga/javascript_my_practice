/**
  * Завдання.
  *
  * Створити об'єкт користувача, який має три властивості:
  * - Ім'я;
  * - Прізвище;
  * - Професія.
  *
  * А також одним методом sayHi, який виводить у консоль повідомлення 'Привіт.'.
  */

let user = {
  firstName : 'John',
  secondName : 'Smit',
  job : 'driver',
  sayHi() {
    console.log(`Hello`);
    return "Hi!"
  }
}

console.log(user.sayHi());