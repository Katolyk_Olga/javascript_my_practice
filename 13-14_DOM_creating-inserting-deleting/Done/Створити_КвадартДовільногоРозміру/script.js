/**
 * Завдання 1.
 *
 * Написати скрипт, який створить квадрат довільного розміру.
 *
 * Розмір квадрата в пікселях отримати інтерактивно за допомогою діалогового вікна prompt.
 *
 * Якщо користувач ввів розмір квадрата в некоректному форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Всі стилі для квадрата встановити через JavaScript за допомогою одного рядка коду.
 *
 * Тип елемента, що описує квадрат - div.
 * Задати новоствореному елементу CSS-клас .square.
 *
 * Квадрат у вигляді стилізованого елемента div необхідно
 * зробити першим та єдиним нащадком body документа.
 */



let size = prompt('Enter the size of square?!');

function createSquare (param) {
    let size = param;

    // while (param === null || isNaN(param) || param <= 0) {
    //     param = prompt('Enter the size of square?!');
    // }

    while (size === null || isNaN(size) || size <= 0) {
         size = prompt('Enter the size of square?!');
    }

    let square = document.createElement('div');

    square.className = 'square';
    square.style.width = square.style.height = size + 'px';
    square.style.backgroundColor = 'green';

    document.body.innerHTML = '';

    document.body.prepend(square);
    
}

createSquare(size);