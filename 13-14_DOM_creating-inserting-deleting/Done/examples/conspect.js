//Creating property

// document.body.myData = {
//     name: 'Ceaser',
//     title: 'Emperor'
// };
// console.log(document.body.myData); // Emperor


// //Changing prototype
// Element.prototype.sayHi = function () {
//     console.log(`Hello, I'm ${this.tagName}`);
// };
// document.documentElement.sayHi(); // Hello, I'm HTML 
// document.body.sayHi(); // Hello, I'm BODY
// console.dir(document.body);


/* 
Working with HTML attributes:
➞ The HTML attribute may differ from its corresponding DOM property, e.g. the style attribute is a 
string, but the style property is an object:
<div id="elem" about="Elephant"></div>
<script>
console.log(elem.getAttribute("about")); // 'Elephant', reading 
elem.setAttribute("Test", 123); // writing
for (let attr of elem.attributes) { // list all attributes 
console.log(`${attr.name} = ${attr.value}`);
}
</script>
<div id="div" style="color:red;font-size:120%">Hello</div>
<script>
console.log(div.getAttribute('style')); // color:red;font-size:120% 
console.log(div.style); // [object CSSStyleDeclaration] 
console.log(div.style.color); // red
</script> */


/* Creating Elements
<ol id="list">
<li>0</li>
<li>1</li>
<li>2</li>
</ol>
<script>
let newLi = document.createElement("li"); 
newLi.innerHTML = "Hello, world!";
list.appendChild(newLi);
</script> */



//Cloning nodes
/* 
<head>
<style>
.console.log {
padding: 15px;
border: 1px solid #d6e9c6; 
border-radius: 4px; 
color: #3c763d; 
background-color: #dff0d8;
}
</style>
</head>
<body>
<div class="console.log" id="div">
<strong>Hi there!</strong> You've read an important message.
</div>
<script>
let div2 = div.cloneNode(true); // clone the message 
div2.querySelector('strong').innerHTML = 'Bye there!'; //
change the clone
div.after(div2); // show the clone after the existing div
</script>
</body> */

//Moving nodes
/* 
<div id="first">First</div>
<div id="second">Second</div>
<script>
// no need to call remove
second.after(first); // take #second and after it - insert 
#first
</script> */