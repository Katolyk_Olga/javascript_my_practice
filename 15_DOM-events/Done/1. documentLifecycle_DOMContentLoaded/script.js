window.addEventListener('load', () => {
  console.log(
    'Спрацювала подія: window.load (Завантажені та опрацьовані зовнішні ресурси: CSS, JavaScript, зображення...)',
  );
});

document.addEventListener('DOMContentLoaded', () => {
  console.log(
    'Спрацювала подія: document.DOMContentLoaded (HTML завантажено на сторінку та тепер він доступний для маніпуляцій).',
  );

  const message = document.querySelector('h1');
  const button = document.querySelector('button');
  let counter = 0;

  button.addEventListener('mousedown', function(event) {
    counter += 1;

    console.log(event);

    console.log(this);

    event.target.closest('button').style.background = 'green';
    message.innerText = `Counter: ${counter}`;
  });
});



