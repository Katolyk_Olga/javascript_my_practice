//Дата та час

//Створення Дати
let now = new Date();
// console.log(now); // shows current date/time
// 0 means 01.01.1970 UTC+0 
//let Jan01_1970 = new Date(1000*60*60*24);
// console.log(Jan01_1970);
// let date = new Date("2018-05-25");
// console.log(date); // Fri May 25 2018 ...
// let date2 = new Date(1960, 0, 1, 12, 43, 43, 213);
// console.log(date2); // 1.01.2011, 02:03:04.567
// new Date(2011, 0, 1); // 1 Jan 2011, 00:00:0

//Зчитування Дати
// let x = now.getDay(); // 0 - 6
// let currDay = now.getDate(); // 1-31
// let currMonth = now.getMonth() + 1 ;// 0-11
// let currYear = now.getFullYear();
// console.log(`Day of the week ${x} / ${currDay}/${currMonth}/${currYear}`); // 25/5/2018
// // the hour in your current time zone 
// console.log(now.getHours());
// // the hour in UTC+0 time zone (London time without daylight savings) 
// console.log(now.getUTCHours())

//Віднімання дат
let start = Date.now(); // milliseconds count from 1 Jan 1970
// do the job
for (let i = 0; i < 10000000; i++) {
    let doSomething = i * i * i;
}
let end = Date.now(); // done
console.log(`The loop took ${end - start} ms`); // subtract numbers, not date