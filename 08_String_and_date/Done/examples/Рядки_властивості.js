//Властивості рядка

console.dir(new String('Test'));

console.log('My \u00A9'.length); // 

let str = 'Hello';
// the first character 
// console.log(str[0]); // H 
// console.log(str.charAt(0)); // H
// // the last character
// console.log(str[str.length - 1]); // o


// for (let char of 'Hello world') {
//     console.log(char); // H,e,l,l,o
// }


let strNew = 'Hi';
strNew[0] = 'h'; // doesn't work 
console.log(strNew[0]); // H


strNew = 'h' + strNew[1]; // replace the string 
console.log(strNew); // hi

