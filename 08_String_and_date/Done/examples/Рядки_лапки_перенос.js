//Об'ява рядків

let single = 'single-quoted';
let double = "double-quoted";
let backticks = `backticks`;


//Зворотні лапки дозволяють використовувавти цілі вирази
//${sum(1, 2)}

function sum(a, b) {
    return a + b;
}
alert(`1 + 2 = ${sum(1, 2)}.`); // 1 + 2 = 3

// Перенос рядка за допомогою backticks
let guestList = `Guests:
* John
* Peter
* Mary
`;
alert(guestList);