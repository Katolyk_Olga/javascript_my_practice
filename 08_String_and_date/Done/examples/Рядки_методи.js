//Методи рядків

//Зміна регістру
let interface = 'Interface';
// console.log(interface.toUpperCase()); // INTERFACE
// console.log(interface.toLowerCase()); // interface
// console.log(interface);



// console.log('Interface'[0].toLowerCase());

// console.log('Interface'[5].toUpperCase());

// const name = "Obivan"; 
// //'O' === 'O'.toUpperCase()
// console.log(name[0] === name[0].toUpperCase());

//Пошук рядка в рядку

//let str = 'Widget with id';
//console.log(str.indexOf('Widget')); // 0, because 'Widget' is found at the beginning 
//console.log(str.indexOf('widget')); // -1, not found, the search is case-sensitive 
//console.log(str.indexOf("id")); // 1, "id" is found at the position 1 (..idget with id) 
// console.log(str.indexOf("id", 2)) // starting the search from position 2

// console.log(str.lastIndexOf("id")); // 12


//Пошук всіх підрядків у рядках

// let str = 'As sly as a fox, as strong as an ox';
// let target = 'as'; // let's look for it

// let pos = 0;

// while (true) {
//     let foundPos = str.indexOf(target, pos);

//     if (foundPos == -1) break;

//     console.log(`Found at ${foundPos}`);

//     pos = foundPos + 1; // continue the search from the next position
// }

//або

// let pos = -1;
// while ((pos = str.indexOf(target, pos + 1)) !== -1) {
//     console.log(`Found at ${pos}`);
// }

//Наявність значень в рядку

// console.log("Midget".includes("id")); // true
// console.log("Midget".includes("id", 3)); // false, there is no "id" searching from position 3

// console.log("Widget".startsWith("Wid")); // true, "Widget" starts with "Wid"
// console.log("Widget".endsWith("get")); // true, "Widget" ends with "get"


//Приклад slice
let str = "stringify";
console.log(str.slice(0, 5)); // 'strin', the substring from 0 to 5 (not including 5) 
// console.log(str.slice(0, 1)); // 's', from 0 to 1, but not including 1, so only character at 0 
// console.log(str.slice(2)); // ringify, from the 2nd position till the end
// console.log(str.slice(-4, -1));
// console.log(str.slice()); // gif, start at the 4th position from the right, end at the 1st from the righ

//Приклад substring
         //012345678  
// let str = "stringify";
// these are same for substring 
// console.log(str.substring(2, 6)); // "ring" 
// console.log(str.substring(6, 2)); // "ring"
// // ...but not for slice:
// console.log(str.slice(2, 6)); // "ring" (the same) 
// console.log(str.slice(6, 2)); // "" (an empty string)
// console.log(str);
// //Приклад substr
// let str = "stringify";
// console.log(str.substr(2, 4)); // ring, from the 2nd position get 4 characters 
// console.log(str.substr(-4, 2)); // gi, from the 4th position get 2 characters

