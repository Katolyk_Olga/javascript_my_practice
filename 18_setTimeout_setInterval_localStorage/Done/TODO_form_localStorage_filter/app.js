/**
 * Таск Менеджер.
 *
 * Використовуючи форму "task-form" створювати список 
 * задач у списку "collection"
 * Елемент списку має бути з класом "collection-item"
 * Всередині елементу списку має знаходитися лінк з 
 * класом "secondary-content" в якому розмістити іконку 
 * з font awesome по кліку на яку таска буде видалятися 
 * зі списку.
 * Clear tasks має очищати весь список
 * Налаштувати фільтрацію тасок у полі "filter" 
 * при набиранні тексту у полі в списку мають 
 * відображатися лише відповідні таски
 * 
 * Advanced
 * 
 * При оновленні сторінки або закриття відкриття браузера список тасок має доступним
 * Використати localStorage 
 * 
 * 
 */

const form = document.querySelector('#task-form');
const clearBtn = document.querySelector('.clear-tasks');
const ul = document.querySelector('.collection');
const filterInput = document.querySelector('#filter')

form.addEventListener('submit', addTask);
clearBtn.addEventListener('click', clearTasks);
ul.addEventListener('click', deleteCertainTask);
filterInput.addEventListener('input', filterTasks);


function addTask(event) {
    event.preventDefault();
    let textTask = event.target.querySelector('#task').value;
    if (textTask) {
        let newTask = document.createElement('li');
        newTask.classList.add('collection-item');
        newTask.textContent = textTask;
        let removeLink = document.createElement('a');
        removeLink.classList.add('secondary-content');
        removeLink.insertAdjacentHTML('beforeend', '<i class="fa fa-remove"></i>');
        newTask.append(removeLink);
        ul.append(newTask);

        addTaskToLocalStorage(textTask);
        
        form.querySelector('#task').value = ""
    }
}

function clearTasks() {
    // if (ul.children.length > 0) {
    //     for (let i = ul.children.length -1; i>=0; i--) {
    //          ul.children[i].remove();
    //     }
    // }

    while (ul.children.length > 0) {
        ul.firstChild.remove();
    }
}

function deleteCertainTask(event) {
    if (event.target.classList.contains('fa-remove')) {
        event.target.closest('li').remove();
    } 
}

function filterTasks(event) {
    
    let letters = event.target.value.toLowerCase();
    
    document.querySelectorAll('.collection-item').forEach((item) => {
        if (!item.textContent.toLowerCase().includes(letters)) {
            item.hidden = true;
        } else {
            item.hidden = false;
        }       
    }) 
}

function addTaskToLocalStorage(localData) {
    let tasks
    
    if (!localStorage.getItem('tasks')) {
        tasks = []
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(localData);

    localStorage.setItem('tasks', JSON.stringify(tasks));
}