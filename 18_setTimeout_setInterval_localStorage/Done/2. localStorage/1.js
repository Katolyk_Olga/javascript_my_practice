/**
 * Завдання 1.
  *
  * При завантаженні сторінки подивитися - чи є в localStorage 
  * значення ключа userName?
  * Якщо є - виводити повідомлення "Hello, userName" на екран,
  * де замість userName має бути вставлено значення 
  * за однойменним ключем в localStorage
  * Якщо значення за таким ключем немає - 
  * запитати у користувача ім'я, модальним вікном, 
  * записати його в localStorage.
  * Після цього вивести повідомлення "Hello, userName" на екран,
  * де замість userName має бути вставлено значення 
  * за однойменним ключем в localStorage
 */

function getName (){
    let userName = localStorage.getItem("userName")
    // console.log(localStorage.getItem("userName"));
    let hHello = document.createElement("h1");

    if (userName) {
        hHello.innerText = `Hello, ${userName}`
    } else {
        let askName = prompt("Enter your name,please!");
        localStorage.setItem("userName", askName);
        hHello.innerText = `Hello, ${askName}`
    }
    document.body.prepend(hHello);    
};
getName();


