/**
 * Завдання 1.
  *
  * Написати програму для нагадувань.
  *
  * Усі модальні вікна реалізувати через alert.
  *
  * Умови:
  * Якщо користувач не ввів повідомлення для нагадування - вивести alert з повідомленням "Ведіть текст нагадування.";
  * Якщо користувач не ввів значення секунд, через скільки потрібно вивести нагадування —
  * вивести alert з повідомленням «Час затримки має бути більше однієї секунди.»;
  * Якщо всі дані введені правильно, при натисканні на кнопку «нагадати» необхідно її блокувати так,
  * щоб повторний клік став можливим після повного завершення поточного нагадування;
  * Після цього повернути початкові значення обох полів;
  * Створювати нагадування, якщо всередині одного з двох елементів input натиснути клавішу Enter;
  * Після завантаження сторінки встановити фокус у текстовий input.
 */

const btn = document.querySelector('button');
let reminder = document.querySelector('input[type = "text"]');
let secInp = document.querySelector('input[type = "number"]');
let mySec = document.querySelector('section');

let handler = function () {
    btn.disabled = true;

    let remText = reminder.value;

    if (!reminder.value) {
        alert("Ведіть текст нагадування.");
    }

    if (secInp.value < 1) {
        alert("Час затримки має бути більше однієї секунди.");
    }

    setTimeout(function () {
        btn.disabled = false;
        console.log(remText);
    }, secInp.value * 1000)
    
    secInp.value = 0;
    reminder.value = '';
    
}

let keyUpHandler = function (event) {
    console.log(event.key);
    if (event.key === 'Enter') {
        handler();
    }
}

btn.onclick = handler;

mySec.addEventListener('keyup', keyUpHandler);
//secInp.addEventListener('keyup', keyUpHandler);

window.addEventListener('load', function () {
    secInp.focus()
});