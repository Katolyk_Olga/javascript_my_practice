/**
 * NaN найчастіше є результатом помилкового математичного вираження,
 * З ним ми розберемося детальніше трохи пізніше.
 */
const error = NaN;//1,3,453, 12421.43, Infinity, NaN
/* null - це семантичне значення для відсутності значення (не плутати з undefined!). */
const nullValue = null;
const notDefinedValue = undefined;

console.log(typeof error);
console.log(typeof nullValue);
console.log(typeof notDefinedValue);

