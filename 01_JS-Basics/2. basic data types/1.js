// "use strict";


// const PI = 2;
// let message = '2';

// console.log(PI + message);
// //console.log(PI);

// //message = Number(message);

// //console.log(message);
// //Number(message);
// console.log(typeof message);


// Про debugger
var dividend = 18;
var numbers = [2, 3, 4, 5, 6, 7, 8, 9];
var idx;

for (idx = 0; idx < numbers.length; idx++) {
  var factor;
  var divisor = numbers[idx];
  debugger
  if (dividend % divisor === 0) {
    factor = true;
  }
  debugger
  if (factor) {
    console.log(divisor + ' is a factor of ' + dividend + '!');
  }
}


// function setData(){
//     var a = 10;
// }
// function calc(){
//     var b = a + 10;
//     console.log(b)
// }



// Оскільки внутрішня функція утворює закриття, ви можете викликати зовнішню функцію 
// та вказати аргументи як для зовнішньої, так і для внутрішньої функції:
// function outside(x) {
//     function inside(y) {
//       return x + y;
//     }
//     return inside;
//   }  
//   const fnInside = outside(3); // Think of it like: give me a function that adds 3 to whatever you give it
//   console.log(fnInside(5)); // 8
//   console.log(outside(3)(5)); // 8


// console.dir(new String(`Test`));
// console.log(`My\u00A9`.length);