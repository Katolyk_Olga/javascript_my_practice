const a = 2;
const b = 3;
const c = a + b;

console.log('c', c);
console.log('c / 2', c / 2);
console.log('c * 2', c * 2);
console.log('c ** 2', c ** 3);

console.log('c % 2', c % 2); // 5 / 2 = 1
