/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */

let keyPressCalc = (event) => {
    console.log(event);

    let pressedKey = event.code.toLowerCase(); 
    let mySpan = document.querySelector(`#${pressedKey}-counter`);

    if (mySpan) { 
        let num = mySpan.textContent;
        mySpan.textContent = +num + 1;
    }
}

let advancedKeyPressCalc = (keyName) => {
    const newSpan = document.createElement('span');
    newSpan.id = keyName.toLowerCase() + "-counter"
    newSpan.textContent = 0;
    document.body.append(newSpan);

    document.addEventListener("keyup", (event)=>{
        let pressedKey = event.code.toLowerCase(); 
        let mySpan = document.querySelector(`#${pressedKey}-counter`);
    
        if (mySpan) { 
            let num = mySpan.textContent;
            mySpan.textContent = +num + 1;
        }
    });
}

advancedKeyPressCalc("KeyQ");

//document.addEventListener("keyup", keyPressCalc);