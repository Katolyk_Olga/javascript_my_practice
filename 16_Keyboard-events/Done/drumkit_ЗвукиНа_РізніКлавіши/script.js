// let collectDiv = document.querySelectorAll(".key");
// console.log(collectDiv);
document.addEventListener("keydown", (event)=>{
    // console.log(event.key);
    // let keyChoise = event.key.toUpperCase();
    // console.log(event.code.slice(-1));
    let keyChoise = event.code.slice(-1);
    // console.log(document.querySelector(`[data-key=${keyChoise}]`));
    let keyDiv = document.querySelector(`[data-key="${keyChoise}"]`);
           
    if (keyDiv) {
        keyDiv.classList.add("playing");
        keyDiv.addEventListener("transitionend", () => {
            keyDiv.classList.remove("playing");
        })
    };
    let audio = document.querySelector(`audio[data-key="${keyChoise}"]`);
    if (audio) {
        audio.play();
    }
} );