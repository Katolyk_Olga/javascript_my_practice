// const number = Math.floor(Math.random() * 6) + 1
// const userInput = +prompt("Загадайте число від 1 до 6")
// if (number === userInput) {
//     console.log("Вітаємо!");
// } else if (number < userInput) {
//     console.log("Менше!");
// } else if (number > userInput) {
//     console.log("Більше!");
// } else {
//     console.log("Загадайте число від 1 до 6");
// }


// Про цикл For
// for (let i = 1; i <= 100; i = i + 1) {
//     console.log(i);
// } 


// Про цикл While
// let i = 1
// while (i <= 100) {
//     console.log(i);
// } 


// Про цикл do
// let i = 1
// do {
//     console.log(i);
//     i++
// } while (i <= 100)


// Про break - ряд зупиниться на 10
// for (let i = 0; i < 100; i++) {
//     console.log(i);  
//     if (i === 10) {
//       break;
//     }
//   }


// Про continue - ряд почнеться з 10
// for (let i = 0; i < 100; i++) {
//     if (i <= 10) {
//         continue;
//     }
//     console.log(i);
// }


// Напишіть програму, яка виводить у консоль усі числа від 100 до 0, які діляться на 5 без залишку, у зворотному порядку.
// for (let i = 100; i >= 0; i = i -= 5) {
//     console.log(i);
// } 


// Порахуйте суму всіх чисел від 1 до 1000
// let sum = 0;
// for (let i = 1; i <= 1000; i++) {
//   sum += i;
// }
// console.log(sum);


// Отримати від користувача число. Якщо число лежить в діапазоні від 0 до 20, вивести в консоль "Вітаю ваше число виграє ${число} виграє"
// Якщо ні- вивести в консоль - "Соррі, з числом не "${число} вдалось виграти"
let num = +prompt("Введіть число:");
if (isNaN(num)) {
    alert("Ви ввели НЕ число!");
} else {
    if (num > 0 && num < 20) {
        console.log(`Вітаю ваше число виграє ${num} виграє`);
    } else {
        console.log(`Соррі, з числом "${num} не вдалося виграти`);
    }
}



