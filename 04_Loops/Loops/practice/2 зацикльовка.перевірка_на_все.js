/**
 * Завдання 2.
  *
  * Користувач повинен ввести два числа.
  * Якщо введене значення не є числом,
  * програма продовжує опитувати користувача до того часу,
  * Поки він не введе число.
  *
  * Коли користувач введе два числа, вивести в консоль повідомлення:
  * «Вітаємо. Введені вами числа: «ПЕРШЕ_ЧИСЛО» і «ДРУГОЕ_ЧИСЛО».».
  */

let num1 = prompt("Num1 ");
let num2 = prompt("Num2 ");

let incorrect = true;

// console.log(parseInt(null)); //NAN
// console.log(parseInt("")); //NAN (пустий рядок)
// console.log(parseInt("             ")); //NAN
// console.log(parseInt("eefeefefefef")); //NAN
// console.log(parseInt("10px")); //10

// console.log(isNaN("10px")); //true
// console.log(isNaN(",000")); //true
// console.log(isNaN(".033")); //false
// console.log(isNaN("..00")); //true
// console.log(isNaN("+300")); //false
// console.log(isNaN(" ")); //false перетворює пробіл на 0;
// console.log(isNaN("0")); //false
// console.log(isNaN(" 0 ")); //false
// console.log(isNaN("null")); //true
// console.log(isNaN(null)); //false перетворює null на 0;

while(incorrect) {
    if (isNaN(parseInt(num1)) || isNaN(num1)) {
        num1 = prompt("Num1 ");
    }
    else if (isNaN(parseInt(num2)) || isNaN(num2)) {
        num2 = prompt("Num2 "); 
    }
    else {
        console.log(`«Вітаємо. Введені вами числа: «${parseInt(num1)}» і «${parseInt(num2)}».».`);
        incorrect = false;
    }
}



// 2 варіант - доопрацювати
// let num1 = prompt("Num1");
// let num2 = prompt("Num2");

// while (isNaN(num1) || isNaN(num2)) {
//     num1 = parsenInt(prompt("Enter first number"));
//     num2 = parsenInt(prompt("Enter second number"));
// };
// console.log(
    // `Welcome! Your first number is ${num1} and second number is ${num2}`
// );