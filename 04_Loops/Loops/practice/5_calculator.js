/**
 * Завдання 5.
  *
  * Написати програму-калькулятор.
  *
  * Програма запитує у користувача три значення:
  * - Перше число;
  * - Друге число;
  * - Математична операція, яку необхідно здійснити над введеними числами.
  *
  * Програма повинна повторно запитувати дані, якщо:
  * - Будь-яке необхідних чисел не відповідають критерію коректного числа;
  * - Математична операція не є однією з: +, -, /, *, **.
  *
  * Якщо всі дані введені правильно, програма обчислює зазначену операцію, і виводить у консоль результат:
  * «Над числами ЧИСЛО_1 і ЧІСЛО_2 була проведена операція ОПЕРАЦІЯ. Результат: РЕЗУЛЬТАТ.
  *
  * Після першого успішного виконання програма повинна запитати, чи є необхідність виконатися ще раз,
  * і повинна починати свою роботу спочатку доти, доки користувач не відповість "Ні.".
  *
  * Коли користувач відмовиться продовжувати роботу програми, програма виводить повідомлення:
  * «✅ Робота завершена.».
 */

let firstNumber = prompt("Enter FN ");
let secondNumber = prompt("Enter SN ");
let operation = prompt("Enter operaotion");
let res;



while (true) {
    if (isNaN(parseInt(firstNumber)) || isNaN(firstNumber)) {
        firstNumber = prompt("Enter FN ");
    }
    else if (isNaN(parseInt(secondNumber)) || isNaN(secondNumber)) {
        secondNumber = prompt("Enter SN ");
    }
    else if (operation !== "+"
        && operation !== "-"
        && operation !== "*"
        && operation !== "**"
        && operation !== "/") {
        operation = prompt("Pls reenter operation");
    }
    else {
        break;
    }
}

switch (operation) {
    case "+":
        res = +firstNumber + +secondNumber;
        break;
    case "-":
        res = firstNumber - secondNumber;
        break;
    case "*":
        res = firstNumber * secondNumber;
        break;
    case "/":
        res = firstNumber / secondNumber;
        break;
    case "**":
        res = firstNumber ** secondNumber;
        break;
}
console.log(`Над числами ${firstNumber} і ${secondNumber} була проведена операція ${operation}. Результат: ${res}`);

