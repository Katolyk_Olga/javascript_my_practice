/**
 * Створіть дерево, яке на кліку на заголовок
 * приховує-показує нащадків:
 *
 *
 * Умови:
 * - Використовувати лише один обробник подій
 * (застосувати делегування)
 * Клік поза текстом заголовка (на порожньому місці)
 * нічого робити не повинен.
 */

let liElements = document.querySelectorAll('li');

const mainUl = document.getElementById('tree');

mainUl.addEventListener('click', function (event) {
    //console.log(event.target.children[0].tagName);
    if(!event.target.children.length) {
        return
    }
    for (const el of event.target.children) {
        if(el.tagName === 'UL') {
            // if (el.style.display === 'none') {
            //     el.style.display = 'block'
            // } else {
            //     el.style.display = 'none';
            // }
            el.style.display = el.style.display === 'none' ? 'block' : 'none'
        } 
    }
    
    

});