/**
 * Делегування.
 */

const board = document.querySelector('.board');

let prevTarget = null;

const handler = event => {
  console.log(event);
  const td = event.target.closest('div');

  if(!td) {
    return;
  }

  if (prevTarget) {
    const defaultCellColor = document.documentElement.style.getPropertyValue(
      '--cellColor',
    );

    prevTarget.innerText = '';
    prevTarget.style.setProperty('--cellColor', defaultCellColor);
  }
  
  td.innerText = 'X';
  td.style.setProperty('--cellColor', 'deepskyblue');

  prevTarget = td;
};

board.addEventListener('click', handler);
