/**
 * Зробіть так, щоб при натисканні на посилання всередині елемента
 * id="contents" користувачу виводилося питання про те,
 * Чи дійсно він хоче залишити сторінку,
 * і якщо він не хоче, то переривати перехід за посиланням.
 *
 *
 * Умови:
 * Вміст #contents може бути завантажений динамічно та
 * присвоєно за допомогою innerHTML.
 * Так що знайти всі посилання та поставити на них
 * Обробники не можна. Використовуйте делегування.
 * Вміст може мати вкладені теги,
 * у тому числі всередині посилань, наприклад,
 * <a href=".."><i>...</i></a>.
 */
