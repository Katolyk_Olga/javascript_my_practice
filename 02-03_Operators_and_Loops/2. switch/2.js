const user = 'simple';

switch (user) {
  case 'admin':
    console.log('Welcome, admin!');
    break;

  case 'simple':
    console.log('Welcome Simple!');
    break;

  default:
    console.log('Welcome!');
}
