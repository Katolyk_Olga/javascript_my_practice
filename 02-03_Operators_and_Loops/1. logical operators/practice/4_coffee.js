/**
 * Завдання 4.
  *
  * Напишіть програму «Кавова машина».
  *
  * Програма приймає монети та готує напої:
  * - Кава за 25 монет;
  * - Капучіно за 50 монет;
  * - Чай за 10 монет.
  *
  * Щоб програма дізналася, що робити, вона повинна знати:
  * - Скільки монет користувач вніс;
  * - Який він бажає напій.
  *
  * Залежно від того, який напій вибрав користувач,
  * програма повинна обчислити здачу та вивести повідомлення в консоль:
  * «Ваш «НАЗВА НАПОЇ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
  *
  * Якщо користувач ввів суму без здачі - вивести повідомлення:
  * «Ваш «НАЗВА НАПОЇ» готовий. Дякую за суму без здачі! :)"
 */

    // Перший варіант:
// let coin = prompt("Enter some coin.");
// let drink = prompt("Choose drink.");
// let drinkCost;

// if (isNaN(coin) || !coin) {
//     alert("You enter not a coin.");
// } else {
//     coin = parseInt(coin);
//     if (drink == "Coffee") {
//         drinkCost = 25;
//         if (coin < drinkCost) {
//             coin = coin + +prompt(`Not enough, add ${drinkCost - coin} coin.`);
//         }
//         if (coin > drinkCost) {
//             console.log(`${drink} is ready. Take your change ${coin - drinkCost}!`);
//         } else if (coin == drinkCost) {
//             console.log(`${drink} is ready. Thank you!`);
//         }
//     } else if (drink == "Cappuccino") {
//         drinkCost = 50;
//         if (coin < drinkCost) {
//             coin = coin + +prompt(`Not enough, add ${drinkCost - coin} coin.`);
//         }
//         if (coin > drinkCost) {
//             console.log(`${drink} is ready. Take your change ${coin - drinkCost}!`);
//         } else if (coin == drinkCost) {
//             console.log(`${drink} is ready. Thank you!`);
//         }
//     } else if (drink == "Tea") {
//         drinkCost = 10;
//         if (coin < drinkCost) {
//             coin = coin + +prompt(`Not enough, add ${drinkCost - coin} coin.`);
//         }
//         if (coin > drinkCost) {
//             console.log(`${drink} is ready. Take your change ${coin - drinkCost}!`);
//         } else if (coin == drinkCost) {
//             console.log(`${drink} is ready. Thank you!`);
//         }
//     } else {
//         console.log("We do not have that drink.");
//     }
// }

    // Другий варіант:
let coin = prompt("Enter some coin.");
if (isNaN(coin) || !coin) {
    alert("You enter not a coin.");
} else {
    let drink = prompt("Choose drink.");
    let drinkCost;
    coin = parseInt(coin);
    switch (drink) {
        case ("Coffee"):
            drinkCost = 25;
            console.log(drink, drinkCost);
            break;
        case ("Cappuccino"):
            drinkCost = 50;
            console.log(drink, drinkCost);
            break;
        case ("Tea"):
            drinkCost = 10;
            console.log(drink, drinkCost);
            break;
        default:
            console.log("We do not have that drink.");
    }
    if (coin < drinkCost) {
        coin = coin + +prompt(`Not enough, add ${drinkCost - coin} coin.`);
    }
    if (coin > drinkCost) {
        console.log(`${drink} is ready. Take your change ${coin - drinkCost}!`);
    } else if (coin == drinkCost) {
        console.log(`${drink} is ready. Thank you!`);
    }
}
