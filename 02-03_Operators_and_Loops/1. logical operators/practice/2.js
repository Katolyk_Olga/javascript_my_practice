/**
 * Завдання 2.
  *
  * Написати програму, яка вітатиме користувача.
  * Спершу користувач вводить своє ім'я, після чого програма виводить у консоль повідомлення
  * з урахуванням його посади.
  *
  * Список посад:
  * Mike - CEO;
  * Jane - CTO;
  * Walter - програміст:
  * Oliver - менеджер;
  * John - прибиральник.
  *
  * Якщо введено не відоме програмі ім'я — вивести в консоль повідомлення «Користувач не знайдено».
  *
  * Виконати завдання у двох варіантах:
  * - використовуючи конструкцію if/else if/else;
  * - Використовуючи конструкцію switch.
 */


let name = prompt("Enter your name");

// if (name == "Mike") {
//     console.log("Hello Mike - CEO");
// }  else if (name == "Jane") {
//     console.log("Hello Jane - CEO");
// } else  if (name == "Walter") {
//     console.log("Hello Walter - CEO");
// } else if (name == "Oliver") {
//     console.log("Hello Oliver - CEO");
// } else if (name == "John") {
//     console.log("Hello John - CEO");
// } else {
//     console.log("incorrect name")
// }
switch (name) {
    case "Mike":
        console.log("Hello Mike - CEO");
        break;
    case "Jane":
        console.log("Hello Jane - CEO");
        break;
    case "Walter":
        console.log("Hello Walter - CEO");
        break;
    case "Oliver":
        console.log("Hello Oliver - CEO");
        break;
    case "John":
        console.log("Hello John - CEO");
        break;
    default:
        console.log("Enter correct name");
}