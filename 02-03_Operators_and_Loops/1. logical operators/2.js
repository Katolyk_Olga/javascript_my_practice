/**
 * Логічний оператор "І" повертає першу брехню.
 */

console.log(true && true); // true
console.log(false && true); // false
console.log(true && false); // false
console.log(false && false); // false
console.log(true && true && false && true); // false
