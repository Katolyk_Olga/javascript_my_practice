// Отримати від користувача 2 числа.
// Якщо перше число лежить у діапазоні від 0 до 20,
// а друге – від 20 до 40 – вивести на сторінку за допомогою document.write
// - "Ви виграєте автомобіль!".
// Якщо хоча б одне з чисел менше 0,
// вивести у модальному вікні напис “А ти хитрун”

let num1 = +prompt("Enter your number")
let num2 = +prompt("Enter second number")

if (isNaN(num1) || isNaN(num2)){
    alert("This is not a number");
} else {
    if (num1 >= 0 && 
        num1 <=20 && 
        num2 >= 20 && 
        num2 <= 40) {
        console.log("You won a car");
    }

    if (num1 < 0 || num2 < 0) {
        console.log("You're a cheater!");
    }
}